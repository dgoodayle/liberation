/*
Needed Mods:
- RHS AFRF
- RHS USAF
- CUP 

Optional Mods:
- None
*/

// Enemy infantry classes
opfor_officer = "CUP_O_TK_Officer";						//Officer
opfor_squad_leader = "CUP_O_TK_Soldier_SL";					//Squad Leader
opfor_team_leader = "CUP_O_TK_Soldier_SL";					//Team Leader
opfor_sentry = "CUP_O_TK_Soldier";						//Rifleman (Lite)
opfor_rifleman = "CUP_O_TK_Soldier";						//Rifleman
opfor_rpg = "CUP_O_TK_Soldier_LAT";						//Rifleman (LAT)
opfor_grenadier = "CUP_O_TK_Soldier";						//Grenadier
opfor_machinegunner = "CUP_O_TK_Soldier";					//Autorifleman
opfor_heavygunner = "CUP_O_TK_Soldier";						//Heavy Gunner
opfor_marksman = "CUP_O_TK_Soldier";						//Marksman
opfor_sharpshooter = "CUP_O_TK_Soldier";					//Sharpshooter
opfor_sniper = "CUP_O_TK_Soldier";						//Sniper
opfor_at = "CUP_O_TK_Soldier_AT";						//AT Specialist
opfor_aa = "CUP_O_TK_Soldier_AA";						//AA Specialist
opfor_medic = "CUP_O_TK_Medic";							//Combat Life Saver
opfor_engineer = "	CUP_O_TK_Engineer";					//Engineer
opfor_paratrooper = "CUP_O_TK_Soldier";

// Enemy vehicles used by secondary objectives.
opfor_mrap = "rhsusf_m1025_w_m2";							//M1025A2 (M2)
opfor_mrap_armed = "rhsusf_m1025_w_m2";							//M1025A2 (M2)
opfor_transport_helo = "RHS_Mi8mt_Cargo_vvsc";						//Mi-8MT (Cargo)
opfor_transport_truck = "RHS_Ural_MSV_01";						//Ural-4320 Transport (Covered)
opfor_ammobox_transport = "RHS_Ural_Open_MSV_01";					//Ural-4320 Transport (Open) -> Has to be able to transport resource crates!
opfor_fuel_truck = "RHS_Ural_Fuel_MSV_01";						//Ural-4320 Fuel
opfor_ammo_truck = "rhs_gaz66_ammo_msv";						//GAZ-66 Ammo
opfor_fuel_container = "Land_Pod_Heli_Transport_04_fuel_F";				//Taru Fuel Pod
opfor_ammo_container = "Land_Pod_Heli_Transport_04_ammo_F";				//Taru Ammo Pod
opfor_flag = "Flag_NATO_F";								//NATO Flag

/* Adding a value to these arrays below will add them to a one out of however many in the array, random pick chance.
Therefore, adding the same value twice or three times means they are more likely to be chosen more often. */

/* Militia infantry. Lightweight soldier classnames the game will pick from randomly as sector defenders.
Think of them like garrison or military police forces, which are more meant to control the local population instead of fighting enemy armies. */
militia_squad = [
	"CUP_O_TK_Soldier",												//Rifleman
	"CUP_O_TK_Soldier",												//Rifleman
	"CUP_O_TK_Soldier",												//Rifleman
	"CUP_O_TK_Soldier",												//Rifleman
	"CUP_O_TK_Soldier",												//Rifleman
	"CUP_O_TK_Soldier",												//Rifleman
	"CUP_O_TK_Soldier_LAT",												//Rifleman (AT)
	"CUP_O_TK_Medic",												//Medic
	"CUP_O_TK_Engineer"												//Engineer
];

// Militia vehicles. Lightweight vehicle classnames the game will pick from randomly as sector defenders.
militia_vehicles = [
	"rhsusf_m1025_w_m2"	,												//M1025A2 (M2)
	"rhsusf_m1025_w_m2"													//M1025A2 (M2)
];

// All enemy vehicles that can spawn as sector defenders and patrols at high enemy combat readiness (aggression levels).
opfor_vehicles = [
	"rhsusf_m1025_w_m2",													//M1025A2 (M2)
	"rhsusf_m1025_w_m2",													//M1025A2 (M2)
	"rhsusf_m1025_w_m2",													//M1025A2 (M2)
	"rhsusf_m1025_w_m2",													//M1025A2 (M2)
	"rhsusf_M1117_W"													//M1117 ASV													
];

// All enemy vehicles that can spawn as sector defenders and patrols but at a lower enemy combat readiness (aggression levels).
opfor_vehicles_low_intensity = [
	"rhsusf_m1025_w_m2"													//M1025A2 (M2)
];

// All enemy vehicles that can spawn as battlegroups, either assaulting or as reinforcements, at high enemy combat readiness (aggression levels).
opfor_battlegroup_vehicles = [
	"rhsusf_m1025_w_m2"	,												//M1025A2 (M2)
	"rhsusf_m1025_w_m2",													//M1025A2 (M2)	
	"rhsusf_M1117_W"													//M1117 ASV													

];

// All enemy vehicles that can spawn as battlegroups, either assaulting or as reinforcements, at lower enemy combat readiness (aggression levels).
opfor_battlegroup_vehicles_low_intensity = [
	"rhsusf_m1025_w_m2"	,
	"rhsusf_m1025_w_m2"													//M1025A2 (M2)
													//M1025A2 (M2)
];

/* All vehicles that spawn within battlegroups (see the above 2 arrays) and also hold 8 soldiers as passengers.
If something in this array can't hold all 8 soldiers then buggy behaviours may occur.	*/
opfor_troup_transports = [
	"RHS_Ural_Open_MSV_01",												//Ural-4320 Transport
	"RHS_Ural_MSV_01"												//Ural-4320 Transport (Covered)
];
// Enemy rotary-wings that will need to spawn in flight.
opfor_choppers = [
	"O_Heli_Transport_04_bench_F",										//Mi-290 Taru (Bench)
	"O_Heli_Light_02_dynamicLoadout_F",									//Po-30 Orca (Armed)
	"O_Heli_Attack_02_dynamicLoadout_F"									//Mi-48 Kajman
];

// Enemy fixed-wings that will need to spawn in the air.
opfor_air = [
	"O_Plane_CAS_02_dynamicLoadout_F",									//To-199 Neophron (CAS)
	"O_Plane_Fighter_02_F"												//To-201 Shikra
];
